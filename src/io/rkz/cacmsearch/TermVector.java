package io.rkz.cacmsearch;

import java.util.Collections;
import java.util.HashMap;


/**
 * Vector of terms weights. Each coordinate represents the weight of a particular word in the document or the query.
 */
public class TermVector extends HashMap<String, Double>
{
    /**
     * Get a vector's coordinate. Coordinates on axis not present in the vector's definition are zero.
     */
    public Double get(String axis)
    {
        if (containsKey(axis)) return super.get(axis);
        else return 0.0;
    }

    /**
     * Normalize a TermVector.
     *
     * @return a copy of the vector where each coordinate is divided by the vector's norm.
     */
    public TermVector normalize()
    {
        double norm = getNorm();

        TermVector normalized = new TermVector();
        for (String word : keySet())
            normalized.put(word, get(word) / norm);

        return normalized;
    }

    /**
     * Scale a TermVector.
     *
     * @return a copy of the original vector where each coordinate is divided by the biggest coordinate (so that in the
     *         resulting vector, each coordinate is <= 1)
     */
    public TermVector unitScale()
    {
        double maxAxis = Collections.max(values());

        TermVector normalized = new TermVector();
        for (String term : keySet())
            normalized.put(term, get(term) / maxAxis);

        return normalized;
    }

    /**
     * Compte a TermVector's norm.
     *
     * @return the vector's norm = sqrt(sum(coordinate^2))
     */
    public double getNorm()
    {
        double squareNorm = 0.0;
        for (double x : values())
            squareNorm += x*x;

        return Math.sqrt(squareNorm);
    }

    /**
     * Compute the scalar product of two TermVector's.
     *
     * @param v1 first vector
     * @param v2 second vector
     * @return the value of v1.v2
     */
    static public double scalarProduct(TermVector v1, TermVector v2)
    {
        double product = 0.0;

        // No need to iterate over the two vector's axis, as the axis which are not present in both vectors will not
        // contribute to the scalar product
        for (String word : v1.keySet()) {
            product += v1.get(word) * v2.get(word);
        }

        return product;
    }

    /**
     * Compute the cosine between two TermVector's.
     *
     * @param v1 first vector
     * @param v2 second vector
     * @return cos(v1, v2)
     */
    static public double cos(TermVector v1, TermVector v2)
    {
        return scalarProduct(v1.normalize(), v2.normalize());
    }
}
