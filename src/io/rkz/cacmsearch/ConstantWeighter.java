package io.rkz.cacmsearch;

import java.util.HashMap;

/**
 * Assigns a constant weight to each term of the document.
 */
public class ConstantWeighter implements Weighter
{
    public TermVector buildTermVector(CacmDatabase db, Integer docID)
    {
        HashMap<String, Integer> docMap = db.getDocumentIndex().get(docID);
        if (docMap == null) return null;

        TermVector vector = new TermVector();
        for (String term : docMap.keySet()) {
            vector.put(term, 1.0);
        }

        return vector;
    }
}
