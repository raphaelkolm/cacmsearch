package io.rkz.cacmsearch;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Set of query test cases with expected results.
 */
public class QuerySuite
{
    private HashMap<Integer, QueryCase> cases;

    public QuerySuite(String queryFilePath, String resultsFilePath) throws IOException
    {
        parseQueryFile(queryFilePath);
        parseResultsFile(resultsFilePath);
    }

    /**
     * Parse a query file (query.text), saving the built {@link QueryCase}s into the QuerySuite's cases map.
     *
     * @param queryFilePath path to the query file
     * @throws IOException
     */
    private void parseQueryFile(String queryFilePath) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(queryFilePath));
        String currentLine;
        QueryCase currentCase = null;
        boolean reading = false;

        cases = new HashMap<Integer, QueryCase>();

        while ((currentLine = br.readLine()) != null) {

            // Query start (.I <id>)
            if (currentLine.startsWith(".I")) {
                if (currentCase != null) cases.put(currentCase.getId(), currentCase);
                int id = Integer.parseInt(currentLine.substring(3).trim());
                currentCase = new QueryCase(id);
            }

            // W field: turn reading on, from next line
            else if (currentLine.startsWith(".W")) {
                reading = true;
            }

            // Other field: do not read until the next field
            else if (currentLine.startsWith(".")) {
                reading = false;
            }

            // Not a field mark: buffer content if reading is turned on
            else if (reading) {
                currentCase.setQuery(currentCase.getQuery() + currentLine);
            }
        }

        if (currentCase != null) cases.put(currentCase.getId(), currentCase);
    }

    private void parseResultsFile(String resultsFilePath) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(resultsFilePath));
        String currentLine;

        while ((currentLine = br.readLine()) != null)
        {
            List<String> parts = Arrays.asList(currentLine.split(" "));
            if (parts.size() < 2) continue;

            int queryID = Integer.parseInt(parts.get(0));
            int expectedDocID = Integer.parseInt(parts.get(1));

            if (!cases.containsKey(queryID)) continue;

            cases.get(queryID).addExpectedDocument(expectedDocID);
        }
    }

    public void runAll(SearchEngine engine)
    {
        for (QueryCase c : cases.values()) {
            c.run(engine);
            System.out.print(".");
        }
        System.out.println("");
    }

    /**
     * Compute average precision over all the queries for recall ranges of recallStep width between 0 and 1.
     *
     * @param recallStep
     */
    public HashMap<Double, Double> computeRecallPrecision(double recallStep)
    {
        // Merge all recall/precision pairs
        ArrayList<Pair<Double, Double>> measures = new ArrayList<Pair<Double, Double>>();
        for (QueryCase c : cases.values()) {
            try {
                measures.addAll(c.getRecallPrecisionMeasures());
            }
            catch (QueryCaseNotRanException e) {}
        }

        // Compute average precision for each recall range
        HashMap<Double, Double> agregatedMeasures = new HashMap<Double, Double>();
        double lowerBound = 0.0;
        while (lowerBound < 1.0) {
            double precisionSum = 0.0;
            int matchingMeasures = 0;
            for (Pair<Double, Double> measure : measures) {
                double recall = measure.getKey();
                if (recall >= lowerBound && recall <= lowerBound + recallStep) {
                    precisionSum += measure.getValue();
                    matchingMeasures++;
                }
            }
            agregatedMeasures.put(lowerBound + recallStep/2, (precisionSum + 0.0) / matchingMeasures);
            lowerBound += recallStep;
        }

        return agregatedMeasures;
    }

    public HashMap<Integer, QueryCase> getCases() {
        return cases;
    }
}
