package io.rkz.cacmsearch;

/**
 * Interface of term vector builders, which implement a weighting method.
 */
public interface Weighter
{
    public TermVector buildTermVector(CacmDatabase db, Integer docID);
}
