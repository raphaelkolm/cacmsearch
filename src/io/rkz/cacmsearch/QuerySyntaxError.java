package io.rkz.cacmsearch;

/**
 * Syntax error in a boolean expression.
 */
public class QuerySyntaxError extends Exception {
}
