package io.rkz.cacmsearch;

import java.util.List;

/**
 * Interface for serach engines.
 */
public interface SearchEngine
{
    public List<SearchMatch> search(String query) throws QuerySyntaxError;
}
