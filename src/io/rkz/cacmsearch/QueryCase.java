package io.rkz.cacmsearch;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Test case of a query with expected results.
 */
public class QueryCase
{
    private Integer id;
    private String query;
    private List<Integer> expectedDocuments;
    private boolean hasRun;
    private double runTime;
    private List<Integer> returnedDocuments;
    private List<Integer> intersection;

    public QueryCase(Integer id)
    {
        this.id = id;
        this.query = "";
        this.expectedDocuments = new ArrayList<Integer>();
    }

    public void run(SearchEngine engine)
    {
        try {
            // Run the query
            double startTime = Main.checkpoint();
            List<SearchMatch> results = engine.search(this.query);
            runTime = Main.checkpoint() - startTime;

            // Save the returned docID's
            returnedDocuments = new ArrayList<Integer>();
            for (SearchMatch match : results) returnedDocuments.add(match.getDocumentID());

            // Compute the expected/returned intersection
            intersection = new ArrayList<Integer>(returnedDocuments);
            intersection.retainAll(expectedDocuments);

            hasRun = true;
        }
        catch (QuerySyntaxError e) {}
    }

    public Integer getId()
    {
        return id;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public void addExpectedDocument(Integer docID)
    {
        expectedDocuments.add(docID);
    }

    /**
     * Computes and returns (recall, precision) couples over the search results.
     *
     * @return
     * @throws QueryCaseNotRanException
     */
    public List<Pair<Double, Double>> getRecallPrecisionMeasures() throws QueryCaseNotRanException
    {
        if (!hasRun) throw new QueryCaseNotRanException();

        ArrayList<Pair<Double, Double>> measures = new ArrayList<Pair<Double, Double>>();
        int relevantCount = 0;

        for (int i = 0; i < returnedDocuments.size(); i++) {
            if (expectedDocuments.contains(returnedDocuments.get(i))) {
                // Checkpoint: compute recall/precision for the (i+1) first results
                relevantCount++;
                double recall = (relevantCount + 0.0) / expectedDocuments.size();
                double precision = (relevantCount + 0.0) / (i + 1);
                measures.add(new Pair<Double, Double>(recall, precision));
            }
        }

        return measures;
    }

    public double getRunTime() throws QueryCaseNotRanException
    {
        if (!hasRun) throw new QueryCaseNotRanException();
        return runTime;
    }
}
