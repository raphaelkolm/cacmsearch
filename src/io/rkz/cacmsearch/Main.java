package io.rkz.cacmsearch;

import java.io.IOException;
import java.util.*;

import javafx.util.Pair;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

public class Main
{
    public static void printUsage(Options options)
    {
        HelpFormatter hf = new HelpFormatter();
        hf.printHelp("cacmsearch <options> \"single query\"\n    or cacmsearch <options> -q<query_suite> -r<expected_results>", options);
    }

    public static double checkpoint()
    {
        return ((new Date()).getTime() + 0.0) / 1000;
    }

    public static void main(String[] args)
    {
        double tStart = checkpoint();

        Options options = new Options();
        options.addOption(OptionBuilder.withArgName("d").withLongOpt("database").isRequired().hasArg().create("d"));
        options.addOption("s", "stoplist", true, "path to the stoplist");
        options.addOption("e", "engine", true, "search engine (boolean, vectorial)");
        options.addOption("w", "weighter", true, "weighter (constant, normalized, tfidf)");
        options.addOption("n", "num-results", true, "number of results to display (defaults to 10)");
        options.addOption("q", "queries", true, "query list file (query.text)");
        options.addOption("r", "rels", true, "expected results for engine evaluation");

        try {
            // Parse command line arguments
            CommandLineParser parser = new PosixParser();
            CommandLine cmd = parser.parse(options, args);

            // Open CACM database
            CacmDatabase db = new CacmDatabase(cmd.getOptionValue("database"), cmd.getOptionValue("stoplist", ""));
            double tIndex = checkpoint();
            System.out.println(String.format("CACM database loaded (%d documents).", db.getDocuments().size()));

            // Initialize the weighter
            Weighter weighter;
            String weighterName = cmd.getOptionValue("weighter", "constant");
            if (weighterName.equals("constant")) {
                weighter = new ConstantWeighter();
            }
            else if (weighterName.equals("normalized")) {
                weighter = new NormalizedFrequencyWeighter();
                weighterName = "normalized frequency";
            }
            else if (weighterName.equals("tfidf")) {
                weighter = new TFIDFWeighter();
                weighterName = "TF-IDF";
            }
            else {
                throw new ParseException(String.format("invalid weighter type '%s'", weighterName));
            }

            // Initialize the search engine
            SearchEngine engine;
            String engineName = cmd.getOptionValue("engine", "vectorial");
            if (engineName.equals("boolean")) {
                engine = new BooleanSearchEngine(db, weighter);
            }
            else if (engineName.equals("vectorial")) {
                engine = new VectorSearchEngine(db, weighter);
            }
            else {
                throw new ParseException(String.format("invalid search engine '%s'", engineName));
            }

            System.out.println(String.format("Using %s engine with %s weighter.", engineName, weighterName));

            // Single query as an argument
            if (!cmd.hasOption("queries"))
            {
                // Run query
                String query = StringUtils.join(cmd.getArgs(), " ");
                System.out.println(String.format("Querying for '%s'.", query));

                List<SearchMatch> results = engine.search(query);
                double tSearch = checkpoint();

                System.out.println(String.format("Done with %d results.", results.size()));
                System.out.println("");

                // Show results
                int numResults = Integer.parseInt(cmd.getOptionValue("num-results", "10"));
                String resultsFormat = "%-5d %-1.2f   %-6d %s";
                System.out.println("Rank  Score  DocID  Title");
                System.out.println(StringUtils.repeat("-", 6 + 11 + 7 + 50));
                for (int i = 0; i < results.size() && i < numResults; i++) {
                    SearchMatch match = results.get(i);
                    System.out.println(String.format(resultsFormat,
                            i,
                            match.getScore(),
                            match.getDocumentID(),
                            db.getDocument(match.getDocumentID()).getTitle()));
                }

                // Print stats
                System.out.println("");
                System.out.println(String.format("Statistics: indexation=%.2f s, search=%.2f s",
                        tIndex - tStart,
                        tSearch - tIndex));
            }

            // Multiple queries to run
            else
            {
                String queryFilePath = cmd.getOptionValue("queries");
                String resultsFilePath = cmd.getOptionValue("rels");

                if (resultsFilePath == null) throw new ParseException("Missing argument: -r or --rels");

                QuerySuite qs = new QuerySuite(queryFilePath, resultsFilePath);
                System.out.println(String.format("Query suite initialized (%d queries).", qs.getCases().size()));

                System.out.print("Running");
                qs.runAll(engine);
                double tSearch = checkpoint();
                System.out.println("");

                HashMap<Double, Double> recallPrecisionMeasures = qs.computeRecallPrecision(0.05);

                System.out.println("Recall;Precision");
                for (Map.Entry<Double, Double> rp : recallPrecisionMeasures.entrySet()) {
                    System.out.println(String.format("%.3f;%.3f", rp.getKey(), rp.getValue()));
                }

                // Print stats
                System.out.println("");
                System.out.println(String.format("Statistics: indexation=%.2f s, queries=%.2f s",
                        tIndex - tStart,
                        tSearch - tIndex));
            }

        }
        catch (ParseException e) {
            System.err.println(e.getMessage());
            printUsage(options);
            System.exit(1);
        }
        catch (QuerySyntaxError e) {
            System.err.println("Query syntax error: " + e.getMessage());
            System.exit(1);
        }
        catch (IOException e) {
            System.err.println(String.format("I/O error: %s", e.getMessage()));
            printUsage(options);
            System.exit(2);
        }
    }
}
