package io.rkz.cacmsearch;

import java.util.*;

/**
 * Performs vectorial searches over a {@link CacmDatabase}.
 */
public class VectorSearchEngine implements SearchEngine
{
    private CacmDatabase database;
    private Weighter weighter;

    /**
     * Construct a VectorSearchEngine over a given index.
     */
    public VectorSearchEngine(CacmDatabase database, Weighter weighter)
    {
        this.database = database;
        this.weighter = weighter;
    }

    /**
     * Perform a search over the index.
     *
     * The search is performed as follows:
     * - Compute cosines between the query vector and each document (using the document index).
     * - Ignore documents which are orthogonal to the query, i.e. irrelevant.
     * - Sort the remaining documents by descending cosine and return the corresponding {@link SearchMatch} set.
     *
     * @param query the query as a TermVector (each coordinate represents the number of occurences of the word in the
     *              query string)
     * @return an ArrayList of relevant matches
     */
    public ArrayList<SearchMatch> search(TermVector query)
    {
        ArrayList<SearchMatch> matches = new ArrayList<SearchMatch>();

        // Compute and keep matches with a non-null relevance
        Set<Integer> docIDs = database.getDocumentIndex().keySet();
        for (int docID : docIDs) {
            TermVector tv = weighter.buildTermVector(database, docID);
            double score = TermVector.cos(tv, query);
            if (score > 0.001) matches.add(new SearchMatch(docID, score));
        }

        // Sort by relevance (most relevant match first)
        Collections.sort(matches);
        Collections.reverse(matches);

        return matches;
    }

    /**
     * Perform a search from a natural language query.
     *
     * @param query natural language query (e.g. "hello world" for "hello" and "world" with weights 1)
     */
    public ArrayList<SearchMatch> search(String query)
    {
        List<String> words = Arrays.asList(query.trim()
            .toLowerCase()
            .replaceAll("[^a-z]", " ")  // replace all non-letters by a space
            .replaceAll("( )+", " ")   // shrink consecutive spaces to 1 space
            .split(" "));

        TermVector v = new TermVector();
        for (String word : words) {
            v.put(word, v.get(word) + 1.0);
        }

        return search(v);
    }
}
