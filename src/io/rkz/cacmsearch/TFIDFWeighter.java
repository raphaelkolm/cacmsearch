package io.rkz.cacmsearch;

import java.util.HashMap;

/**
 * TF-IDF weighter
 */
public class TFIDFWeighter implements Weighter
{
    public TermVector buildTermVector(CacmDatabase db, Integer docID)
    {
        HashMap<String, Integer> docMap = db.getDocumentIndex().get(docID);
        if (docMap == null) return null;

        TermVector tv = new TermVector();
        Integer docCount = db.getDocuments().size();

        for (String term : docMap.keySet())
        {
            // Compute term's TF
            double TF;
            Integer freq = docMap.get(term);
            if (freq == 0) TF = 0;
            else TF = 1 + Math.log10(freq);

            // Compute term's IDF
            double IDF;
            Integer docsWithTerm = 0;  // number of documents containing the term
            HashMap<Integer, Integer> termMap = db.getTermIndex().get(term);
            for (Integer id : termMap.keySet()) {
                if (termMap.get(id) != 0) docsWithTerm++;
            }
            if (docsWithTerm > 0) IDF = Math.log10(docCount / docsWithTerm);
            else IDF = 0;

            tv.put(term, TF * IDF);
        }

        return tv.unitScale();
    }
}
