package io.rkz.cacmsearch.test;

import io.rkz.cacmsearch.TermVector;
import junit.framework.TestCase;

/**
 * Test case for TermVector
 */
public class TermVectorTestCase extends TestCase
{
    public void testCoordinates()
    {
        TermVector c1 = new TermVector();
        c1.put("foo", 3.0);
        c1.put("car", 2.0);
        c1.put("wee", 1.5);

        assertEquals(c1.get("foo"), 3.0);
        assertEquals(c1.get("bar"), 0.0);
    }

    public void testScalarProduct()
    {
        TermVector c1 = new TermVector();
        c1.put("foo", 3.0);
        c1.put("car", 2.0);
        c1.put("wee", 1.5);

        TermVector c2 = new TermVector();
        c2.put("bar", 4.0);
        c2.put("wee", 2.0);
        c2.put("zak", 1.1);

        assertEquals(TermVector.scalarProduct(c1, c2), 3.0);
    }

    public void testNormalize()
    {
        TermVector c1 = new TermVector();
        c1.put("foo", 3.0);
        c1.put("car", 2.0);
        c1.put("wee", 1.5);

        TermVector n1 = c1.normalize();
        assertEquals(Math.floor(n1.getNorm()*1000), 1000.0);
        assertEquals(n1.get("bar"), 0.0);
    }

    public void testCos()
    {
        TermVector c1 = new TermVector();
        c1.put("x", 1.0);

        TermVector c2 = new TermVector();
        c2.put("y", 2.0);
        c2.put("z", 1.0);

        TermVector c3 = new TermVector();
        c3.put("x", 1.0);
        c3.put("y", 1.0);

        assertEquals(TermVector.cos(c1, c2), 0.0);
        assertEquals(Math.floor(TermVector.cos(c1, c3)*1000), Math.floor(Math.sqrt(2)/2*1000));
    }
}
