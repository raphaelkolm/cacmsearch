package io.rkz.cacmsearch;

import java.util.Collections;
import java.util.HashMap;

/**
 * Normalized frequency weighter.
 *
 * A term's weight for a document D is computed as follows:
 *
 *    w(term, D) = freq(term, D) / max(freq(t, D) for each term t of D)
 *
 * The most frequent term of D has a weight of 1.
 */
public class NormalizedFrequencyWeighter implements Weighter
{
    public TermVector buildTermVector(CacmDatabase db, Integer docID)
    {
        HashMap<String, Integer> docMap = db.getDocumentIndex().get(docID);
        if (docMap == null) return null;

        double maxFreq = Collections.max(docMap.values());

        TermVector vector = new TermVector();
        for (String term : docMap.keySet()) {
            vector.put(term, docMap.get(term) / maxFreq);
        }

        return vector;
    }
}
