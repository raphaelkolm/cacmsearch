package io.rkz.cacmsearch;

/**
 * Negation of a boolean expression.
 */
public class BooleanNot implements BooleanExpression
{
    BooleanExpression argument;

    public BooleanNot (BooleanExpression argument)
    {
        this.argument = argument;
    }

    public BooleanExpression getArgument() {
        return argument;
    }

    public boolean eval(TermVector vector)
    {
        return !argument.eval(vector);
    }
}
