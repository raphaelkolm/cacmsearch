package io.rkz.cacmsearch;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Represents a CACM document.
 */
public class Document
{
    private int id;
    private String title;
    private ArrayList<String> terms;

    public Document(int id)
    {
        this.id = id;
        this.terms = new ArrayList<String>();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Add multiple terms to the document
     *
     * @param newTerms ArrayList of terms to add
     */
    public void addTerms(ArrayList<String> newTerms) {
        terms.addAll(newTerms);
    }

    /**
     * Get the term frequencies in the document.
     *
     * @return a HashMap of (term -> number of occurences)
     */
    public HashMap<String, Integer> getTermFrequencies()
    {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (String term : terms) {
            if (map.containsKey(term)) map.put(term, map.get(term) + 1);
            else map.put(term, 1);
        }
        return map;
    }
}
